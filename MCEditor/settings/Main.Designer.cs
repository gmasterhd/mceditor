﻿namespace MCEditor.settings {
    partial class Main {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btn_export = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.cbx_er = new MetroFramework.Controls.MetroCheckBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.cbx_elt = new MetroFramework.Controls.MetroCheckBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.cbx_eadv = new MetroFramework.Controls.MetroCheckBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cbx_bt = new MetroFramework.Controls.MetroCheckBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.cbx_eft = new MetroFramework.Controls.MetroCheckBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.cbx_ef = new MetroFramework.Controls.MetroCheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tbx_resourcepack = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_browseR = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rb_resourcepack = new MetroFramework.Controls.MetroRadioButton();
            this.rb_world = new MetroFramework.Controls.MetroRadioButton();
            this.rb_ingore = new MetroFramework.Controls.MetroRadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tbx_wordPath = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_browse = new MaterialSkin.Controls.MaterialFlatButton();
            this.folderBrowser = new Ookii.Dialogs.WinForms.VistaFolderBrowserDialog();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(20, 979);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1880, 82);
            this.panel1.TabIndex = 0;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl1.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.metroTabControl1.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1880, 919);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabControl1.TabIndex = 1;
            this.metroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.panel16);
            this.metroTabPage1.Controls.Add(this.panel15);
            this.metroTabPage1.Controls.Add(this.panel14);
            this.metroTabPage1.Controls.Add(this.panel13);
            this.metroTabPage1.Controls.Add(this.panel12);
            this.metroTabPage1.Controls.Add(this.panel11);
            this.metroTabPage1.Controls.Add(this.panel10);
            this.metroTabPage1.Controls.Add(this.panel6);
            this.metroTabPage1.Controls.Add(this.panel5);
            this.metroTabPage1.Controls.Add(this.panel2);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 44);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1872, 871);
            this.metroTabPage1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Export";
            this.metroTabPage1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(0, 805);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1872, 66);
            this.panel16.TabIndex = 12;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.panel17.Controls.Add(this.btn_export);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel17.Location = new System.Drawing.Point(1682, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(190, 66);
            this.panel17.TabIndex = 13;
            // 
            // btn_export
            // 
            this.btn_export.AutoSize = true;
            this.btn_export.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_export.Depth = 0;
            this.btn_export.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_export.Location = new System.Drawing.Point(0, 0);
            this.btn_export.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_export.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_export.Name = "btn_export";
            this.btn_export.Primary = false;
            this.btn_export.Size = new System.Drawing.Size(190, 66);
            this.btn_export.TabIndex = 0;
            this.btn_export.Text = "Export";
            this.btn_export.UseVisualStyleBackColor = true;
            this.btn_export.Click += new System.EventHandler(this.Btn_export_Click);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel15.Controls.Add(this.cbx_er);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 373);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1872, 41);
            this.panel15.TabIndex = 11;
            // 
            // cbx_er
            // 
            this.cbx_er.AutoSize = true;
            this.cbx_er.Checked = true;
            this.cbx_er.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_er.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_er.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_er.Location = new System.Drawing.Point(0, 0);
            this.cbx_er.Name = "cbx_er";
            this.cbx_er.Size = new System.Drawing.Size(1872, 41);
            this.cbx_er.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_er.TabIndex = 0;
            this.cbx_er.Text = "Export Recipes";
            this.cbx_er.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_er.UseSelectable = true;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel14.Controls.Add(this.cbx_elt);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 332);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1872, 41);
            this.panel14.TabIndex = 10;
            // 
            // cbx_elt
            // 
            this.cbx_elt.AutoSize = true;
            this.cbx_elt.Checked = true;
            this.cbx_elt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_elt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_elt.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_elt.Location = new System.Drawing.Point(0, 0);
            this.cbx_elt.Name = "cbx_elt";
            this.cbx_elt.Size = new System.Drawing.Size(1872, 41);
            this.cbx_elt.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_elt.TabIndex = 0;
            this.cbx_elt.Text = "Export Loot Tables";
            this.cbx_elt.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_elt.UseSelectable = true;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel13.Controls.Add(this.cbx_eadv);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 291);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1872, 41);
            this.panel13.TabIndex = 9;
            // 
            // cbx_eadv
            // 
            this.cbx_eadv.AutoSize = true;
            this.cbx_eadv.Checked = true;
            this.cbx_eadv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_eadv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_eadv.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_eadv.Location = new System.Drawing.Point(0, 0);
            this.cbx_eadv.Name = "cbx_eadv";
            this.cbx_eadv.Size = new System.Drawing.Size(1872, 41);
            this.cbx_eadv.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_eadv.TabIndex = 0;
            this.cbx_eadv.Text = "Export Advancements";
            this.cbx_eadv.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_eadv.UseSelectable = true;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel12.Controls.Add(this.cbx_bt);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 250);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1872, 41);
            this.panel12.TabIndex = 8;
            // 
            // cbx_bt
            // 
            this.cbx_bt.AutoSize = true;
            this.cbx_bt.Checked = true;
            this.cbx_bt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_bt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_bt.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_bt.Location = new System.Drawing.Point(0, 0);
            this.cbx_bt.Name = "cbx_bt";
            this.cbx_bt.Size = new System.Drawing.Size(1872, 41);
            this.cbx_bt.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_bt.TabIndex = 0;
            this.cbx_bt.Text = "Export Block Tags";
            this.cbx_bt.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_bt.UseSelectable = true;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel11.Controls.Add(this.cbx_eft);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 209);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1872, 41);
            this.panel11.TabIndex = 7;
            // 
            // cbx_eft
            // 
            this.cbx_eft.AutoSize = true;
            this.cbx_eft.Checked = true;
            this.cbx_eft.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_eft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_eft.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_eft.Location = new System.Drawing.Point(0, 0);
            this.cbx_eft.Name = "cbx_eft";
            this.cbx_eft.Size = new System.Drawing.Size(1872, 41);
            this.cbx_eft.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_eft.TabIndex = 0;
            this.cbx_eft.Text = "Export Function Tags";
            this.cbx_eft.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_eft.UseSelectable = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel10.Controls.Add(this.cbx_ef);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 168);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1872, 41);
            this.panel10.TabIndex = 6;
            // 
            // cbx_ef
            // 
            this.cbx_ef.AutoSize = true;
            this.cbx_ef.Checked = true;
            this.cbx_ef.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_ef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbx_ef.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbx_ef.Location = new System.Drawing.Point(0, 0);
            this.cbx_ef.Name = "cbx_ef";
            this.cbx_ef.Size = new System.Drawing.Size(1872, 41);
            this.cbx_ef.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_ef.TabIndex = 0;
            this.cbx_ef.Text = "Export Functions";
            this.cbx_ef.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_ef.UseSelectable = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.rb_resourcepack);
            this.panel6.Controls.Add(this.rb_world);
            this.panel6.Controls.Add(this.rb_ingore);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 42);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1872, 126);
            this.panel6.TabIndex = 4;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tbx_resourcepack);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 85);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1872, 31);
            this.panel8.TabIndex = 5;
            // 
            // tbx_resourcepack
            // 
            // 
            // 
            // 
            this.tbx_resourcepack.CustomButton.Image = null;
            this.tbx_resourcepack.CustomButton.Location = new System.Drawing.Point(1518, 1);
            this.tbx_resourcepack.CustomButton.Name = "";
            this.tbx_resourcepack.CustomButton.Size = new System.Drawing.Size(29, 29);
            this.tbx_resourcepack.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_resourcepack.CustomButton.TabIndex = 1;
            this.tbx_resourcepack.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_resourcepack.CustomButton.UseSelectable = true;
            this.tbx_resourcepack.CustomButton.Visible = false;
            this.tbx_resourcepack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_resourcepack.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_resourcepack.Lines = new string[0];
            this.tbx_resourcepack.Location = new System.Drawing.Point(252, 0);
            this.tbx_resourcepack.MaxLength = 32767;
            this.tbx_resourcepack.Name = "tbx_resourcepack";
            this.tbx_resourcepack.PasswordChar = '\0';
            this.tbx_resourcepack.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_resourcepack.SelectedText = "";
            this.tbx_resourcepack.SelectionLength = 0;
            this.tbx_resourcepack.SelectionStart = 0;
            this.tbx_resourcepack.ShortcutsEnabled = true;
            this.tbx_resourcepack.Size = new System.Drawing.Size(1548, 31);
            this.tbx_resourcepack.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_resourcepack.TabIndex = 7;
            this.tbx_resourcepack.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_resourcepack.UseSelectable = true;
            this.tbx_resourcepack.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_resourcepack.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 29);
            this.label2.TabIndex = 6;
            this.label2.Text = "Rsouecepack Folder:";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.panel9.Controls.Add(this.btn_browseR);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(1800, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(72, 31);
            this.panel9.TabIndex = 5;
            // 
            // btn_browseR
            // 
            this.btn_browseR.AutoSize = true;
            this.btn_browseR.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_browseR.Depth = 0;
            this.btn_browseR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_browseR.Location = new System.Drawing.Point(0, 0);
            this.btn_browseR.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_browseR.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_browseR.Name = "btn_browseR";
            this.btn_browseR.Primary = false;
            this.btn_browseR.Size = new System.Drawing.Size(72, 31);
            this.btn_browseR.TabIndex = 3;
            this.btn_browseR.Text = "...";
            this.btn_browseR.UseVisualStyleBackColor = true;
            this.btn_browseR.Click += new System.EventHandler(this.Btn_browseR_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 75);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1872, 10);
            this.panel7.TabIndex = 4;
            // 
            // rb_resourcepack
            // 
            this.rb_resourcepack.AutoSize = true;
            this.rb_resourcepack.Dock = System.Windows.Forms.DockStyle.Top;
            this.rb_resourcepack.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.rb_resourcepack.Location = new System.Drawing.Point(0, 50);
            this.rb_resourcepack.Name = "rb_resourcepack";
            this.rb_resourcepack.Size = new System.Drawing.Size(1872, 25);
            this.rb_resourcepack.Style = MetroFramework.MetroColorStyle.Green;
            this.rb_resourcepack.TabIndex = 2;
            this.rb_resourcepack.Text = "Pack Resourcepack into your Resourcepacks Folder";
            this.rb_resourcepack.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.rb_resourcepack.UseSelectable = true;
            this.rb_resourcepack.CheckedChanged += new System.EventHandler(this.Rb_resourcepack_CheckedChanged);
            // 
            // rb_world
            // 
            this.rb_world.AutoSize = true;
            this.rb_world.Dock = System.Windows.Forms.DockStyle.Top;
            this.rb_world.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.rb_world.Location = new System.Drawing.Point(0, 25);
            this.rb_world.Name = "rb_world";
            this.rb_world.Size = new System.Drawing.Size(1872, 25);
            this.rb_world.Style = MetroFramework.MetroColorStyle.Green;
            this.rb_world.TabIndex = 1;
            this.rb_world.Text = "Pack Resourcepack into the world (resources.zip)";
            this.rb_world.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.rb_world.UseSelectable = true;
            this.rb_world.CheckedChanged += new System.EventHandler(this.Rb_world_CheckedChanged);
            // 
            // rb_ingore
            // 
            this.rb_ingore.AutoSize = true;
            this.rb_ingore.Dock = System.Windows.Forms.DockStyle.Top;
            this.rb_ingore.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.rb_ingore.Location = new System.Drawing.Point(0, 0);
            this.rb_ingore.Name = "rb_ingore";
            this.rb_ingore.Size = new System.Drawing.Size(1872, 25);
            this.rb_ingore.Style = MetroFramework.MetroColorStyle.Green;
            this.rb_ingore.TabIndex = 0;
            this.rb_ingore.Text = "Ignore Resourcepack";
            this.rb_ingore.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.rb_ingore.UseSelectable = true;
            this.rb_ingore.CheckedChanged += new System.EventHandler(this.Rb_ingore_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 32);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1872, 10);
            this.panel5.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1872, 32);
            this.panel2.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tbx_wordPath);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(145, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1655, 32);
            this.panel4.TabIndex = 6;
            // 
            // tbx_wordPath
            // 
            // 
            // 
            // 
            this.tbx_wordPath.CustomButton.Image = null;
            this.tbx_wordPath.CustomButton.Location = new System.Drawing.Point(1625, 2);
            this.tbx_wordPath.CustomButton.Name = "";
            this.tbx_wordPath.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tbx_wordPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_wordPath.CustomButton.TabIndex = 1;
            this.tbx_wordPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_wordPath.CustomButton.UseSelectable = true;
            this.tbx_wordPath.CustomButton.Visible = false;
            this.tbx_wordPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_wordPath.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_wordPath.Lines = new string[0];
            this.tbx_wordPath.Location = new System.Drawing.Point(0, 0);
            this.tbx_wordPath.MaxLength = 32767;
            this.tbx_wordPath.Name = "tbx_wordPath";
            this.tbx_wordPath.PasswordChar = '\0';
            this.tbx_wordPath.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_wordPath.SelectedText = "";
            this.tbx_wordPath.SelectionLength = 0;
            this.tbx_wordPath.SelectionStart = 0;
            this.tbx_wordPath.ShortcutsEnabled = true;
            this.tbx_wordPath.Size = new System.Drawing.Size(1655, 32);
            this.tbx_wordPath.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_wordPath.TabIndex = 0;
            this.tbx_wordPath.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_wordPath.UseSelectable = true;
            this.tbx_wordPath.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_wordPath.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "World Path:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.panel3.Controls.Add(this.btn_browse);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1800, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(72, 32);
            this.panel3.TabIndex = 4;
            // 
            // btn_browse
            // 
            this.btn_browse.AutoSize = true;
            this.btn_browse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_browse.Depth = 0;
            this.btn_browse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_browse.Location = new System.Drawing.Point(0, 0);
            this.btn_browse.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_browse.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Primary = false;
            this.btn_browse.Size = new System.Drawing.Size(72, 32);
            this.btn_browse.TabIndex = 3;
            this.btn_browse.Text = "...";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.Btn_browse_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "Main";
            this.Padding = new System.Windows.Forms.Padding(20, 60, 20, 19);
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "McEditor: Settings";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MaterialSkin.Controls.MaterialFlatButton btn_browse;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private MetroFramework.Controls.MetroTextBox tbx_wordPath;
        private Ookii.Dialogs.WinForms.VistaFolderBrowserDialog folderBrowser;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private MetroFramework.Controls.MetroRadioButton rb_resourcepack;
        private MetroFramework.Controls.MetroRadioButton rb_world;
        private MetroFramework.Controls.MetroRadioButton rb_ingore;
        private MetroFramework.Controls.MetroTextBox tbx_resourcepack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel9;
        private MaterialSkin.Controls.MaterialFlatButton btn_browseR;
        private System.Windows.Forms.Panel panel15;
        private MetroFramework.Controls.MetroCheckBox cbx_er;
        private System.Windows.Forms.Panel panel14;
        private MetroFramework.Controls.MetroCheckBox cbx_elt;
        private System.Windows.Forms.Panel panel13;
        private MetroFramework.Controls.MetroCheckBox cbx_eadv;
        private System.Windows.Forms.Panel panel12;
        private MetroFramework.Controls.MetroCheckBox cbx_bt;
        private System.Windows.Forms.Panel panel11;
        private MetroFramework.Controls.MetroCheckBox cbx_eft;
        private System.Windows.Forms.Panel panel10;
        private MetroFramework.Controls.MetroCheckBox cbx_ef;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private MaterialSkin.Controls.MaterialFlatButton btn_export;
    }
}