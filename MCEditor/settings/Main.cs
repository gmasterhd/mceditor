﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCEditor.settings {
    public partial class Main : MetroFramework.Forms.MetroForm {
        public Main() {
            InitializeComponent();

            tbx_wordPath.Text = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\.minecraft\saves";
            tbx_resourcepack.Text = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\.minecraft\resourcepacks";

            rb_world.Checked = true;
            cbx_elt.Checked = true;
        }
        private void Btn_browse_Click(object sender, EventArgs e) {
            folderBrowser.SelectedPath = tbx_wordPath.Text;
            folderBrowser.ShowDialog();

            if(folderBrowser.SelectedPath != "") {
                tbx_wordPath.Text = folderBrowser.SelectedPath;
            }
        }

        private void Btn_browseR_Click(object sender, EventArgs e) {
            folderBrowser.SelectedPath = tbx_resourcepack.Text;
            folderBrowser.ShowDialog();

            if(folderBrowser.SelectedPath != "") {
                tbx_resourcepack.Text = folderBrowser.SelectedPath;
            }
        }

        private void Rb_ingore_CheckedChanged(object sender, EventArgs e) {
            updateRadio();
        }
        private void Rb_world_CheckedChanged(object sender, EventArgs e) {
            updateRadio();
        }
        private void Rb_resourcepack_CheckedChanged(object sender, EventArgs e) {
            updateRadio();
        }

        private void updateRadio() {
            if(rb_resourcepack.Checked == true) {
                tbx_resourcepack.Enabled = true;
                btn_browseR.Enabled = true;
            } else {
                tbx_resourcepack.Enabled = false;
                btn_browseR.Enabled = false;
            }
        }

        private void Btn_export_Click(object sender, EventArgs e) {
            if(cbx_ef.Checked == true) {
                // -> Export Functions <- //
            }
            if(cbx_eadv.Checked == true) {
                // -> Export Advancements <- //
            }
            if(cbx_eft.Checked == true) {
                // -> Export Function Tags <- //
            }
            if(cbx_elt.Checked == true) {
                // -> Export Loot Tables <- //
            }
            if(cbx_er.Checked == true) {
                // -> Export Recipes <- //
            }
        }
    }
}
