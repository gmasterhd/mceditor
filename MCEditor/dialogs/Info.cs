﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCEditor.dialogs {
    public partial class Info : MetroFramework.Forms.MetroForm {
        public Info() {
            InitializeComponent();
        }
        
        public void show(string title, string body) {
            this.tbx_title.Text = title;
            this.tbx_body.Text = body;
            this.Visible = true;
        }

        private void Info_Load(object sender, EventArgs e) {
        }

        private void Btn_ok_Click(object sender, EventArgs e) {
            this.Visible = false;
        }
    }
}
