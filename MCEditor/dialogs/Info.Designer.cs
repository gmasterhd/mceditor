﻿namespace MCEditor.dialogs {
    partial class Info {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbx_title = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tbx_body = new MetroFramework.Controls.MetroTextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_ok = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 289);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbx_title);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(276, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(709, 35);
            this.panel2.TabIndex = 1;
            // 
            // tbx_title
            // 
            // 
            // 
            // 
            this.tbx_title.CustomButton.Image = null;
            this.tbx_title.CustomButton.Location = new System.Drawing.Point(675, 1);
            this.tbx_title.CustomButton.Name = "";
            this.tbx_title.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.tbx_title.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_title.CustomButton.TabIndex = 1;
            this.tbx_title.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_title.CustomButton.UseSelectable = true;
            this.tbx_title.CustomButton.Visible = false;
            this.tbx_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_title.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_title.Lines = new string[] {
        "This is an Info!"};
            this.tbx_title.Location = new System.Drawing.Point(0, 0);
            this.tbx_title.MaxLength = 32767;
            this.tbx_title.Name = "tbx_title";
            this.tbx_title.PasswordChar = '\0';
            this.tbx_title.ReadOnly = true;
            this.tbx_title.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_title.SelectedText = "";
            this.tbx_title.SelectionLength = 0;
            this.tbx_title.SelectionStart = 0;
            this.tbx_title.ShortcutsEnabled = true;
            this.tbx_title.Size = new System.Drawing.Size(709, 35);
            this.tbx_title.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_title.TabIndex = 0;
            this.tbx_title.Text = "This is an Info!";
            this.tbx_title.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_title.UseSelectable = true;
            this.tbx_title.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_title.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MCEditor.Properties.Resources.info;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.ErrorImage = global::MCEditor.Properties.Resources.info;
            this.pictureBox1.InitialImage = global::MCEditor.Properties.Resources.info;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(276, 292);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(709, 57);
            this.panel3.TabIndex = 2;
            // 
            // tbx_body
            // 
            // 
            // 
            // 
            this.tbx_body.CustomButton.Image = null;
            this.tbx_body.CustomButton.Location = new System.Drawing.Point(513, 1);
            this.tbx_body.CustomButton.Name = "";
            this.tbx_body.CustomButton.Size = new System.Drawing.Size(195, 195);
            this.tbx_body.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_body.CustomButton.TabIndex = 1;
            this.tbx_body.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_body.CustomButton.UseSelectable = true;
            this.tbx_body.CustomButton.Visible = false;
            this.tbx_body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_body.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_body.Lines = new string[] {
        "This is an Info!"};
            this.tbx_body.Location = new System.Drawing.Point(276, 95);
            this.tbx_body.MaxLength = 32767;
            this.tbx_body.Multiline = true;
            this.tbx_body.Name = "tbx_body";
            this.tbx_body.PasswordChar = '\0';
            this.tbx_body.ReadOnly = true;
            this.tbx_body.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_body.SelectedText = "";
            this.tbx_body.SelectionLength = 0;
            this.tbx_body.SelectionStart = 0;
            this.tbx_body.ShortcutsEnabled = true;
            this.tbx_body.Size = new System.Drawing.Size(709, 197);
            this.tbx_body.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_body.TabIndex = 3;
            this.tbx_body.Text = "This is an Info!";
            this.tbx_body.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_body.UseSelectable = true;
            this.tbx_body.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_body.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.panel4.Controls.Add(this.btn_ok);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(479, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(230, 57);
            this.panel4.TabIndex = 0;
            // 
            // btn_ok
            // 
            this.btn_ok.AutoSize = true;
            this.btn_ok.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_ok.Depth = 0;
            this.btn_ok.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_ok.Location = new System.Drawing.Point(0, 0);
            this.btn_ok.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_ok.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Primary = false;
            this.btn_ok.Size = new System.Drawing.Size(230, 57);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "Ok";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.Btn_ok_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(256, 256);
            this.panel5.TabIndex = 1;
            // 
            // Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 369);
            this.Controls.Add(this.tbx_body);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Info";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Info_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private MetroFramework.Controls.MetroTextBox tbx_title;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private MaterialSkin.Controls.MaterialFlatButton btn_ok;
        private MetroFramework.Controls.MetroTextBox tbx_body;
        private System.Windows.Forms.Panel panel5;
    }
}