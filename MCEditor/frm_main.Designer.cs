﻿namespace MCEditor {
    partial class frm_main {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.pnl_menu = new System.Windows.Forms.Panel();
            this.pnl_settings = new System.Windows.Forms.Panel();
            this.btn_settings = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnl_generators = new System.Windows.Forms.Panel();
            this.btn_generators = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnl_snippets = new System.Windows.Forms.Panel();
            this.btn_snippets = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnl_file = new System.Windows.Forms.Panel();
            this.btn_file = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnl_edit = new System.Windows.Forms.Panel();
            this.btn_edit = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tv_explorer = new System.Windows.Forms.TreeView();
            this.ctm_fileExplorer = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.tsm_open = new System.Windows.Forms.ToolStripMenuItem();
            this.visualStudioTabControl1 = new VisualStudioTabControl.VisualStudioTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbx_code = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.fbd = new Ookii.Dialogs.WinForms.VistaFolderBrowserDialog();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.pnl_menu.SuspendLayout();
            this.pnl_settings.SuspendLayout();
            this.pnl_generators.SuspendLayout();
            this.pnl_snippets.SuspendLayout();
            this.pnl_file.SuspendLayout();
            this.pnl_edit.SuspendLayout();
            this.panel2.SuspendLayout();
            this.ctm_fileExplorer.SuspendLayout();
            this.visualStudioTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbx_code)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_menu
            // 
            this.pnl_menu.Controls.Add(this.pnl_settings);
            this.pnl_menu.Controls.Add(this.pnl_generators);
            this.pnl_menu.Controls.Add(this.pnl_snippets);
            this.pnl_menu.Controls.Add(this.pnl_file);
            this.pnl_menu.Controls.Add(this.pnl_edit);
            this.pnl_menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_menu.Location = new System.Drawing.Point(20, 60);
            this.pnl_menu.Name = "pnl_menu";
            this.pnl_menu.Size = new System.Drawing.Size(256, 1000);
            this.pnl_menu.TabIndex = 0;
            // 
            // pnl_settings
            // 
            this.pnl_settings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.pnl_settings.Controls.Add(this.btn_settings);
            this.pnl_settings.Location = new System.Drawing.Point(0, 303);
            this.pnl_settings.Name = "pnl_settings";
            this.pnl_settings.Size = new System.Drawing.Size(255, 67);
            this.pnl_settings.TabIndex = 3;
            // 
            // btn_settings
            // 
            this.btn_settings.AutoSize = true;
            this.btn_settings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_settings.Depth = 0;
            this.btn_settings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_settings.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_settings.Location = new System.Drawing.Point(0, 0);
            this.btn_settings.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_settings.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_settings.Name = "btn_settings";
            this.btn_settings.Primary = false;
            this.btn_settings.Size = new System.Drawing.Size(255, 67);
            this.btn_settings.TabIndex = 0;
            this.btn_settings.Text = "Settings";
            this.btn_settings.UseVisualStyleBackColor = true;
            this.btn_settings.Click += new System.EventHandler(this.Btn_settings_Click);
            // 
            // pnl_generators
            // 
            this.pnl_generators.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.pnl_generators.Controls.Add(this.btn_generators);
            this.pnl_generators.Location = new System.Drawing.Point(0, 219);
            this.pnl_generators.Name = "pnl_generators";
            this.pnl_generators.Size = new System.Drawing.Size(255, 67);
            this.pnl_generators.TabIndex = 2;
            // 
            // btn_generators
            // 
            this.btn_generators.AutoSize = true;
            this.btn_generators.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_generators.Depth = 0;
            this.btn_generators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_generators.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_generators.Location = new System.Drawing.Point(0, 0);
            this.btn_generators.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_generators.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_generators.Name = "btn_generators";
            this.btn_generators.Primary = false;
            this.btn_generators.Size = new System.Drawing.Size(255, 67);
            this.btn_generators.TabIndex = 0;
            this.btn_generators.Text = "Generators";
            this.btn_generators.UseVisualStyleBackColor = true;
            this.btn_generators.Click += new System.EventHandler(this.Btn_generators_Click);
            // 
            // pnl_snippets
            // 
            this.pnl_snippets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.pnl_snippets.Controls.Add(this.btn_snippets);
            this.pnl_snippets.Location = new System.Drawing.Point(0, 146);
            this.pnl_snippets.Name = "pnl_snippets";
            this.pnl_snippets.Size = new System.Drawing.Size(255, 67);
            this.pnl_snippets.TabIndex = 1;
            // 
            // btn_snippets
            // 
            this.btn_snippets.AutoSize = true;
            this.btn_snippets.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_snippets.Depth = 0;
            this.btn_snippets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_snippets.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_snippets.Location = new System.Drawing.Point(0, 0);
            this.btn_snippets.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_snippets.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_snippets.Name = "btn_snippets";
            this.btn_snippets.Primary = false;
            this.btn_snippets.Size = new System.Drawing.Size(255, 67);
            this.btn_snippets.TabIndex = 0;
            this.btn_snippets.Text = "Snippets";
            this.btn_snippets.UseVisualStyleBackColor = true;
            this.btn_snippets.Click += new System.EventHandler(this.Btn_snippets_Click);
            // 
            // pnl_file
            // 
            this.pnl_file.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.pnl_file.Controls.Add(this.btn_file);
            this.pnl_file.Location = new System.Drawing.Point(0, 0);
            this.pnl_file.Name = "pnl_file";
            this.pnl_file.Size = new System.Drawing.Size(255, 67);
            this.pnl_file.TabIndex = 0;
            // 
            // btn_file
            // 
            this.btn_file.AutoSize = true;
            this.btn_file.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_file.Depth = 0;
            this.btn_file.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_file.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_file.Location = new System.Drawing.Point(0, 0);
            this.btn_file.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_file.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_file.Name = "btn_file";
            this.btn_file.Primary = false;
            this.btn_file.Size = new System.Drawing.Size(255, 67);
            this.btn_file.TabIndex = 0;
            this.btn_file.Text = "File";
            this.btn_file.UseVisualStyleBackColor = true;
            this.btn_file.Click += new System.EventHandler(this.Btn_file_Click);
            // 
            // pnl_edit
            // 
            this.pnl_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.pnl_edit.Controls.Add(this.btn_edit);
            this.pnl_edit.Location = new System.Drawing.Point(0, 73);
            this.pnl_edit.Name = "pnl_edit";
            this.pnl_edit.Size = new System.Drawing.Size(255, 67);
            this.pnl_edit.TabIndex = 1;
            // 
            // btn_edit
            // 
            this.btn_edit.AutoSize = true;
            this.btn_edit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_edit.Depth = 0;
            this.btn_edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_edit.Location = new System.Drawing.Point(0, 0);
            this.btn_edit.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_edit.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Primary = false;
            this.btn_edit.Size = new System.Drawing.Size(255, 67);
            this.btn_edit.TabIndex = 0;
            this.btn_edit.Text = "Edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.Btn_edit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tv_explorer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(276, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(410, 1000);
            this.panel2.TabIndex = 0;
            // 
            // tv_explorer
            // 
            this.tv_explorer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tv_explorer.ContextMenuStrip = this.ctm_fileExplorer;
            this.tv_explorer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv_explorer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.tv_explorer.Location = new System.Drawing.Point(0, 0);
            this.tv_explorer.Name = "tv_explorer";
            this.tv_explorer.Size = new System.Drawing.Size(410, 1000);
            this.tv_explorer.TabIndex = 0;
            this.tv_explorer.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Tv_explorer_AfterSelect);
            this.tv_explorer.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Tv_explorer_MouseDoubleClick);
            // 
            // ctm_fileExplorer
            // 
            this.ctm_fileExplorer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_open});
            this.ctm_fileExplorer.Name = "ctm_fileExplorer";
            this.ctm_fileExplorer.Size = new System.Drawing.Size(104, 26);
            // 
            // tsm_open
            // 
            this.tsm_open.Name = "tsm_open";
            this.tsm_open.Size = new System.Drawing.Size(103, 22);
            this.tsm_open.Text = "Open";
            this.tsm_open.Click += new System.EventHandler(this.Tsm_open_Click);
            // 
            // visualStudioTabControl1
            // 
            this.visualStudioTabControl1.ActiveColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.visualStudioTabControl1.AllowDrop = true;
            this.visualStudioTabControl1.BackTabColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.visualStudioTabControl1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.visualStudioTabControl1.ClosingButtonColor = System.Drawing.Color.WhiteSmoke;
            this.visualStudioTabControl1.ClosingMessage = null;
            this.visualStudioTabControl1.Controls.Add(this.tabPage1);
            this.visualStudioTabControl1.Controls.Add(this.tabPage2);
            this.visualStudioTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visualStudioTabControl1.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.visualStudioTabControl1.HorizontalLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.visualStudioTabControl1.ItemSize = new System.Drawing.Size(240, 16);
            this.visualStudioTabControl1.Location = new System.Drawing.Point(686, 60);
            this.visualStudioTabControl1.Multiline = true;
            this.visualStudioTabControl1.Name = "visualStudioTabControl1";
            this.visualStudioTabControl1.SelectedIndex = 0;
            this.visualStudioTabControl1.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.visualStudioTabControl1.ShowClosingButton = true;
            this.visualStudioTabControl1.ShowClosingMessage = false;
            this.visualStudioTabControl1.Size = new System.Drawing.Size(1214, 1000);
            this.visualStudioTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.visualStudioTabControl1.TabIndex = 1;
            this.visualStudioTabControl1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tabPage1.Controls.Add(this.tbx_code);
            this.tabPage1.Location = new System.Drawing.Point(4, 20);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1206, 976);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // tbx_code
            // 
            this.tbx_code.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.tbx_code.AutoScrollMinSize = new System.Drawing.Size(39, 54);
            this.tbx_code.BackBrush = null;
            this.tbx_code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tbx_code.CaretColor = System.Drawing.Color.White;
            this.tbx_code.CharHeight = 27;
            this.tbx_code.CharWidth = 14;
            this.tbx_code.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbx_code.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.tbx_code.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_code.Font = new System.Drawing.Font("Courier New", 18F);
            this.tbx_code.ForeColor = System.Drawing.Color.White;
            this.tbx_code.IndentBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tbx_code.IsReplaceMode = false;
            this.tbx_code.LineNumberColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.tbx_code.Location = new System.Drawing.Point(3, 3);
            this.tbx_code.Name = "tbx_code";
            this.tbx_code.Paddings = new System.Windows.Forms.Padding(0);
            this.tbx_code.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.tbx_code.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("tbx_code.ServiceColors")));
            this.tbx_code.Size = new System.Drawing.Size(1200, 970);
            this.tbx_code.TabIndex = 0;
            this.tbx_code.Text = "\r\n";
            this.tbx_code.Zoom = 100;
            this.tbx_code.TextChanged += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.tbx_codeTextChanged);
            this.tbx_code.Load += new System.EventHandler(this.Tbx_code_Load);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 20);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1206, 976);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.visualStudioTabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnl_menu);
            this.Name = "frm_main";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "MCEditor by GMasterHD";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Frm_main_Load);
            this.pnl_menu.ResumeLayout(false);
            this.pnl_settings.ResumeLayout(false);
            this.pnl_settings.PerformLayout();
            this.pnl_generators.ResumeLayout(false);
            this.pnl_generators.PerformLayout();
            this.pnl_snippets.ResumeLayout(false);
            this.pnl_snippets.PerformLayout();
            this.pnl_file.ResumeLayout(false);
            this.pnl_file.PerformLayout();
            this.pnl_edit.ResumeLayout(false);
            this.pnl_edit.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ctm_fileExplorer.ResumeLayout(false);
            this.visualStudioTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbx_code)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_menu;
        private System.Windows.Forms.Panel pnl_generators;
        private MaterialSkin.Controls.MaterialFlatButton btn_generators;
        private System.Windows.Forms.Panel pnl_snippets;
        private MaterialSkin.Controls.MaterialFlatButton btn_snippets;
        private System.Windows.Forms.Panel pnl_edit;
        private MaterialSkin.Controls.MaterialFlatButton btn_edit;
        private System.Windows.Forms.Panel pnl_file;
        private MaterialSkin.Controls.MaterialFlatButton btn_file;
        private System.Windows.Forms.Panel panel2;
        private VisualStudioTabControl.VisualStudioTabControl visualStudioTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private FastColoredTextBoxNS.FastColoredTextBox tbx_code;
        private System.Windows.Forms.Panel pnl_settings;
        private MaterialSkin.Controls.MaterialFlatButton btn_settings;
        public System.Windows.Forms.TreeView tv_explorer;
        private Ookii.Dialogs.WinForms.VistaFolderBrowserDialog fbd;
        private MetroFramework.Controls.MetroContextMenu ctm_fileExplorer;
        private System.Windows.Forms.ToolStripMenuItem tsm_open;
        private System.Windows.Forms.OpenFileDialog ofd;
    }
}

