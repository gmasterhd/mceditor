﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCEditor.file {
    public partial class NewFile : MetroFramework.Forms.MetroForm {
        public NewFile() {
            InitializeComponent();

            cbx_type.SelectedIndex = 0;
        }

        private void Btn_create_Click(object sender, EventArgs e) {
            if(tbx_nameSpace.Text == "") {
                new MCEditor.dialogs.Info().show("Unset value", "You have to set the name of your file!");
            } else if(tbx_file.Text == "") {
                new MCEditor.dialogs.Info().show("Unset value", "You have to set the namespace, where your file will be saved in!");
            } else {
                if(cbx_type.SelectedIndex == 0) {
                    Console.WriteLine("File of type " + cbx_type.SelectedItem + "(" + cbx_type.SelectedIndex + ") in " + tbx_nameSpace.Text + ":" + tbx_file.Text + ".mcfunction");

                    TreeNode node;

                    if (tbx_file.Text.Contains("\\")) {
                        node = frm_main.instance.tv_explorer.Nodes.Add(frm_main.explorerRoot + "\\Datapacks\\Namespaces\\" + tbx_nameSpace.Text + "\\" + tbx_file.Text.Substring(0, tbx_file.Text.LastIndexOf("\\")));
                    } else {
                        node = frm_main.instance.tv_explorer.Nodes.Add(frm_main.explorerRoot + "\\Datapacks\\Namespaces\\" + tbx_nameSpace.Text + "\\" + tbx_file.Text);
                    }

                    node.Nodes.Add(tbx_file.Text + "\\.mcfunction");

                    System.IO.Directory.CreateDirectory(frm_main.explorerRoot + "\\Datapack\\Namespaces\\" + tbx_nameSpace.Text);
                    System.IO.File.WriteAllText(frm_main.explorerRoot + "\\Datapack\\Namespaces\\" + tbx_nameSpace.Text + "\\" + tbx_file.Text + ".mcfunction", "");
                    frm_main.instance.open(frm_main.explorerRoot + "\\Datapack\\Namespaces\\" + tbx_nameSpace.Text + "\\" + tbx_file.Text + ".mcfunction");
                } else if (cbx_type.SelectedIndex == 1) {
                    Console.WriteLine("File of type " + cbx_type.SelectedItem + "(" + cbx_type.SelectedIndex + ") in " + tbx_nameSpace.Text + ":" + tbx_file.Text + ".json");
                } else if (cbx_type.SelectedIndex == 2) {
                    Console.WriteLine("File of type " + cbx_type.SelectedItem + "(" + cbx_type.SelectedIndex + ") in " + tbx_nameSpace.Text + ":" + tbx_file.Text + ".json");
                }

                this.Visible = false;
            }
        }

        private void Btn_abbort_Click(object sender, EventArgs e) {
            this.Visible = false;
        }

        private void Cbx_type_SelectedIndexChanged(object sender, EventArgs e) {
        }
    }
}
