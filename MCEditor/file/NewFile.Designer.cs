﻿namespace MCEditor.file {
    partial class NewFile {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cbx_type = new MetroFramework.Controls.MetroComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbx_file = new MetroFramework.Controls.MetroTextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tbx_nameSpace = new MetroFramework.Controls.MetroTextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_create = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_abbort = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbx_type
            // 
            this.cbx_type.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbx_type.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cbx_type.FormattingEnabled = true;
            this.cbx_type.ItemHeight = 29;
            this.cbx_type.Items.AddRange(new object[] {
            "Function (.mcfunction)",
            "Advancement (.json)",
            "Tag (.json)"});
            this.cbx_type.Location = new System.Drawing.Point(20, 60);
            this.cbx_type.Name = "cbx_type";
            this.cbx_type.Size = new System.Drawing.Size(955, 35);
            this.cbx_type.Style = MetroFramework.MetroColorStyle.Green;
            this.cbx_type.TabIndex = 0;
            this.cbx_type.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbx_type.UseSelectable = true;
            this.cbx_type.SelectedIndexChanged += new System.EventHandler(this.Cbx_type_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(20, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 13);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbx_file);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(20, 108);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(955, 33);
            this.panel2.TabIndex = 2;
            // 
            // tbx_file
            // 
            // 
            // 
            // 
            this.tbx_file.CustomButton.Image = null;
            this.tbx_file.CustomButton.Location = new System.Drawing.Point(633, 1);
            this.tbx_file.CustomButton.Name = "";
            this.tbx_file.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.tbx_file.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_file.CustomButton.TabIndex = 1;
            this.tbx_file.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_file.CustomButton.UseSelectable = true;
            this.tbx_file.CustomButton.Visible = false;
            this.tbx_file.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_file.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_file.Lines = new string[0];
            this.tbx_file.Location = new System.Drawing.Point(290, 0);
            this.tbx_file.MaxLength = 32767;
            this.tbx_file.Name = "tbx_file";
            this.tbx_file.PasswordChar = '\0';
            this.tbx_file.PromptText = "File";
            this.tbx_file.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_file.SelectedText = "";
            this.tbx_file.SelectionLength = 0;
            this.tbx_file.SelectionStart = 0;
            this.tbx_file.ShortcutsEnabled = true;
            this.tbx_file.Size = new System.Drawing.Size(665, 33);
            this.tbx_file.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_file.TabIndex = 2;
            this.tbx_file.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_file.UseSelectable = true;
            this.tbx_file.WaterMark = "File";
            this.tbx_file.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_file.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(270, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(20, 33);
            this.panel4.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = ":";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tbx_nameSpace);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(270, 33);
            this.panel3.TabIndex = 0;
            // 
            // tbx_nameSpace
            // 
            // 
            // 
            // 
            this.tbx_nameSpace.CustomButton.Image = null;
            this.tbx_nameSpace.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.tbx_nameSpace.CustomButton.Name = "";
            this.tbx_nameSpace.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.tbx_nameSpace.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbx_nameSpace.CustomButton.TabIndex = 1;
            this.tbx_nameSpace.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbx_nameSpace.CustomButton.UseSelectable = true;
            this.tbx_nameSpace.CustomButton.Visible = false;
            this.tbx_nameSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_nameSpace.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tbx_nameSpace.Lines = new string[0];
            this.tbx_nameSpace.Location = new System.Drawing.Point(0, 0);
            this.tbx_nameSpace.MaxLength = 32767;
            this.tbx_nameSpace.Name = "tbx_nameSpace";
            this.tbx_nameSpace.PasswordChar = '\0';
            this.tbx_nameSpace.PromptText = "Namespace";
            this.tbx_nameSpace.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_nameSpace.SelectedText = "";
            this.tbx_nameSpace.SelectionLength = 0;
            this.tbx_nameSpace.SelectionStart = 0;
            this.tbx_nameSpace.ShortcutsEnabled = true;
            this.tbx_nameSpace.Size = new System.Drawing.Size(270, 33);
            this.tbx_nameSpace.Style = MetroFramework.MetroColorStyle.Green;
            this.tbx_nameSpace.TabIndex = 0;
            this.tbx_nameSpace.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbx_nameSpace.UseSelectable = true;
            this.tbx_nameSpace.WaterMark = "Namespace";
            this.tbx_nameSpace.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbx_nameSpace.WaterMarkFont = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(20, 141);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(955, 247);
            this.panel5.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 185);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(955, 62);
            this.panel6.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(177)))), ((int)(((byte)(89)))));
            this.panel8.Controls.Add(this.btn_create);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(515, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(220, 62);
            this.panel8.TabIndex = 1;
            // 
            // btn_create
            // 
            this.btn_create.AutoSize = true;
            this.btn_create.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_create.Depth = 0;
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_create.Location = new System.Drawing.Point(0, 0);
            this.btn_create.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_create.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_create.Name = "btn_create";
            this.btn_create.Primary = false;
            this.btn_create.Size = new System.Drawing.Size(220, 62);
            this.btn_create.TabIndex = 1;
            this.btn_create.Text = "Create";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Click += new System.EventHandler(this.Btn_create_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(17)))), ((int)(((byte)(65)))));
            this.panel7.Controls.Add(this.btn_abbort);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(735, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(220, 62);
            this.panel7.TabIndex = 0;
            // 
            // btn_abbort
            // 
            this.btn_abbort.AutoSize = true;
            this.btn_abbort.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_abbort.Depth = 0;
            this.btn_abbort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_abbort.Location = new System.Drawing.Point(0, 0);
            this.btn_abbort.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_abbort.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_abbort.Name = "btn_abbort";
            this.btn_abbort.Primary = false;
            this.btn_abbort.Size = new System.Drawing.Size(220, 62);
            this.btn_abbort.TabIndex = 0;
            this.btn_abbort.Text = "Abbort";
            this.btn_abbort.UseVisualStyleBackColor = true;
            this.btn_abbort.Click += new System.EventHandler(this.Btn_abbort_Click);
            // 
            // NewFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 408);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbx_type);
            this.Name = "NewFile";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "MCEditor: New File";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cbx_type;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private MetroFramework.Controls.MetroTextBox tbx_nameSpace;
        private MetroFramework.Controls.MetroTextBox tbx_file;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private MaterialSkin.Controls.MaterialFlatButton btn_abbort;
        private MaterialSkin.Controls.MaterialFlatButton btn_create;
    }
}