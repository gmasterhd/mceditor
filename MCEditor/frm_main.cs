﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FastColoredTextBoxNS;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;

namespace MCEditor {
    public partial class frm_main : MetroFramework.Forms.MetroForm {
        bool fileOpen = false;
        bool editOpen = false;
        bool snippetsOpen = false;
        bool generatorsOpen = false;

        public static frm_main instance = new frm_main();

        private Panel pnl_fileMenu = new Panel();
        private Panel pnl_editMenu = new Panel();

        public static string explorerRoot = @"C:\Users\GMasterHD\AppData\Roaming\.minecraft\saves";

        #region FileMenu
        private MaterialSkin.Controls.MaterialFlatButton btn_file_newFile = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_file_openFile = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_file_closeFile = new MaterialSkin.Controls.MaterialFlatButton();

        private MaterialSkin.Controls.MaterialFlatButton btn_file_save = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_file_saveAs = new MaterialSkin.Controls.MaterialFlatButton();

        private MaterialSkin.Controls.MaterialFlatButton btn_file_newWorkspace = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_file_openWorkspace = new MaterialSkin.Controls.MaterialFlatButton();

        private MaterialSkin.Controls.MaterialFlatButton btn_file_exit = new MaterialSkin.Controls.MaterialFlatButton();

        private Panel pnl_file_newFile = new Panel();
        private Panel pnl_file_openFile = new Panel();
        private Panel pnl_file_closeFile = new Panel();

        private Panel pnl_file_save = new Panel();
        private Panel pnl_file_saveAs = new Panel();

        private Panel pnl_file_newWorkspace = new Panel();
        private Panel pnl_file_openWorkspace = new Panel();

        private Panel pnl_file_exit = new Panel();
        #endregion
        #region EditMenu
        private MaterialSkin.Controls.MaterialFlatButton btn_edit_undo = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_edit_redo = new MaterialSkin.Controls.MaterialFlatButton();

        private MaterialSkin.Controls.MaterialFlatButton btn_edit_markAll = new MaterialSkin.Controls.MaterialFlatButton();

        private MaterialSkin.Controls.MaterialFlatButton btn_edit_copy = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_edit_paste = new MaterialSkin.Controls.MaterialFlatButton();
        private MaterialSkin.Controls.MaterialFlatButton btn_edit_cut = new MaterialSkin.Controls.MaterialFlatButton();

        private Panel pnl_edit_undo = new Panel();
        private Panel pnl_edit_redo = new Panel();

        private Panel pnl_edit_markAll = new Panel();

        private Panel pnl_edit_copy = new Panel();
        private Panel pnl_edit_paste = new Panel();
        private Panel pnl_edit_cut = new Panel();
        #endregion

        #region FCTB
        dynamic syntaxHighlighterJson;
        private static List<Style> styles = new List<Style>();
        #endregion

        public frm_main() {
            InitializeComponent();
        }

        private void Frm_main_Load(object sender, EventArgs e) {
            initMenu();
        }

        private void Btn_file_Click(object sender, EventArgs e) {
            if (!fileOpen) {
                openFileMenu();
            } else {
                closeFileMenu();
            }
        }

        private void Btn_edit_Click(object sender, EventArgs e) {
            if (!editOpen) {
                openEditMenu();
            } else {
                closeEditMenu();
            }
        }

        private void Btn_snippets_Click(object sender, EventArgs e) {
            if (snippetsOpen) {
                snippetsOpen = false;
            } else {
                snippetsOpen = true;
            }
        }

        private void Btn_generators_Click(object sender, EventArgs e) {
            if (generatorsOpen) {
                generatorsOpen = false;
            } else {
                generatorsOpen = true;
            }
        }
        private void Btn_settings_Click(object sender, EventArgs e) {
            new MCEditor.settings.Main().Visible = true;
        }

        private void openFileMenu() {
            fileOpen = true;
            editOpen = false;
            snippetsOpen = false;
            generatorsOpen = false;

            pnl_fileMenu.Visible = true;

            pnl_edit.Location = new Point(0, 653);
            pnl_snippets.Location = new Point(0, 720);
            pnl_generators.Location = new Point(0, 787);
            pnl_settings.Location = new Point(0, 845);
        }
        private void closeFileMenu() {
            fileOpen = false;

            pnl_fileMenu.Visible = false;
            pnl_editMenu.Visible = false;

            pnl_file.Location = new Point(0, 0);
            pnl_edit.Location = new Point(0, 67);
            pnl_snippets.Location = new Point(0, 134);
            pnl_generators.Location = new Point(0, 201);
            pnl_settings.Location = new Point(0, 268);
        }

        private void openEditMenu() {
            closeFileMenu();

            fileOpen = false;
            editOpen = true;
            snippetsOpen = false;
            generatorsOpen = false;

            pnl_editMenu.Visible = true;

            pnl_snippets.Location = new Point(0, 576);
            pnl_generators.Location = new Point(0, 643);
            pnl_settings.Location = new Point(0, 710);
        }
        private void closeEditMenu() {
            editOpen = false;

            pnl_editMenu.Visible = false;

            pnl_snippets.Location = new Point(0, 134);
            pnl_generators.Location = new Point(0, 201);
            pnl_settings.Location = new Point(0, 268);
        }

        private void initMenu() {
            pnl_file.Location = new Point(0, 0);
            pnl_edit.Location = new Point(0, 67);
            pnl_snippets.Location = new Point(0, 134);
            pnl_generators.Location = new Point(0, 201);
            pnl_settings.Location = new Point(0, 268);

            #region FileMenu
            pnl_fileMenu.Parent = pnl_menu;
            pnl_fileMenu.Location = new Point(0, 77);
            pnl_fileMenu.Size = new Size(255, 643);
            pnl_fileMenu.Visible = false;

            pnl_file_newFile.Location = new Point(0, 0);
            pnl_file_newFile.Size = new Size(255, 67);
            pnl_file_newFile.Parent = pnl_fileMenu;
            pnl_file_newFile.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_newFile.Parent = pnl_file_newFile;
            btn_file_newFile.Location = new Point(0, 0);
            btn_file_newFile.Text = "New File...";
            btn_file_newFile.Dock = DockStyle.Fill;
            btn_file_newFile.Click += new EventHandler(this.Btn_file_newFile);

            pnl_file_openFile.Location = new Point(0, 67);
            pnl_file_openFile.Size = new Size(255, 67);
            pnl_file_openFile.Parent = pnl_fileMenu;
            pnl_file_openFile.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_openFile.Parent = pnl_file_openFile;
            btn_file_openFile.Location = new Point(0, 0);
            btn_file_openFile.Text = "Open File...";
            btn_file_openFile.Dock = DockStyle.Fill;

            pnl_file_closeFile.Location = new Point(0, 134);
            pnl_file_closeFile.Size = new Size(255, 67);
            pnl_file_closeFile.Parent = pnl_fileMenu;
            pnl_file_closeFile.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_closeFile.Parent = pnl_file_closeFile;
            btn_file_closeFile.Location = new Point(0, 0);
            btn_file_closeFile.Text = "Close File";
            btn_file_closeFile.Dock = DockStyle.Fill;

            pnl_file_save.Location = new Point(0, 211);
            pnl_file_save.Size = new Size(255, 67);
            pnl_file_save.Parent = pnl_fileMenu;
            pnl_file_save.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_save.Parent = pnl_file_save;
            btn_file_save.Location = new Point(0, 0);
            btn_file_save.Text = "Save";
            btn_file_save.Dock = DockStyle.Fill;

            pnl_file_saveAs.Location = new Point(0, 278);
            pnl_file_saveAs.Size = new Size(255, 67);
            pnl_file_saveAs.Parent = pnl_fileMenu;
            pnl_file_saveAs.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_saveAs.Parent = pnl_file_saveAs;
            btn_file_saveAs.Location = new Point(0, 0);
            btn_file_saveAs.Text = "Save as...";
            btn_file_saveAs.Dock = DockStyle.Fill;

            pnl_file_newWorkspace.Location = new Point(0, 355);
            pnl_file_newWorkspace.Size = new Size(255, 67);
            pnl_file_newWorkspace.Parent = pnl_fileMenu;
            pnl_file_newWorkspace.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_newWorkspace.Parent = pnl_file_newWorkspace;
            btn_file_newWorkspace.Location = new Point(0, 0);
            btn_file_newWorkspace.Text = "New Workspace...";
            btn_file_newWorkspace.Click += new EventHandler(this.Btn_file_newWorkspace);
            btn_file_newWorkspace.Dock = DockStyle.Fill;

            pnl_file_openWorkspace.Location = new Point(0, 422);
            pnl_file_openWorkspace.Size = new Size(255, 67);
            pnl_file_openWorkspace.Parent = pnl_fileMenu;
            pnl_file_openWorkspace.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_openWorkspace.Parent = pnl_file_openWorkspace;
            btn_file_openWorkspace.Location = new Point(0, 0);
            btn_file_openWorkspace.Text = "Open Workspace...";
            btn_file_openWorkspace.Click += new EventHandler(this.Btn_file_openWorkspace);
            btn_file_openWorkspace.Dock = DockStyle.Fill;

            pnl_file_exit.Location = new Point(0, 499);
            pnl_file_exit.Size = new Size(255, 67);
            pnl_file_exit.Parent = pnl_fileMenu;
            pnl_file_exit.BackColor = Color.FromArgb(40, 40, 40);
            btn_file_exit.Parent = pnl_file_exit;
            btn_file_exit.Location = new Point(0, 0);
            btn_file_exit.Text = "Exit";
            btn_file_exit.Dock = DockStyle.Fill;
            #endregion
            #region EditMenu
            pnl_editMenu.Parent = pnl_menu;
            pnl_editMenu.Location = new Point(0, 144);
            pnl_editMenu.Size = new Size(255, 422);
            pnl_editMenu.Visible = false;

            pnl_edit_undo.Location = new Point(0, 0);
            pnl_edit_undo.Size = new Size(255, 67);
            pnl_edit_undo.Parent = pnl_editMenu;
            pnl_edit_undo.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_undo.Parent = pnl_edit_undo;
            btn_edit_undo.Location = new Point(0, 0);
            btn_edit_undo.Text = "Undo";
            btn_edit_undo.Dock = DockStyle.Fill;

            pnl_edit_redo.Location = new Point(0, 67);
            pnl_edit_redo.Size = new Size(255, 67);
            pnl_edit_redo.Parent = pnl_editMenu;
            pnl_edit_redo.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_redo.Parent = pnl_edit_redo;
            btn_edit_redo.Location = new Point(0, 0);
            btn_edit_redo.Text = "Redo";
            btn_edit_redo.Dock = DockStyle.Fill;

            pnl_edit_markAll.Location = new Point(0, 144);
            pnl_edit_markAll.Size = new Size(255, 67);
            pnl_edit_markAll.Parent = pnl_editMenu;
            pnl_edit_markAll.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_markAll.Parent = pnl_edit_markAll;
            btn_edit_markAll.Location = new Point(0, 0);
            btn_edit_markAll.Text = "Mark All";
            btn_edit_markAll.Dock = DockStyle.Fill;

            pnl_edit_copy.Location = new Point(0, 221);
            pnl_edit_copy.Size = new Size(255, 67);
            pnl_edit_copy.Parent = pnl_editMenu;
            pnl_edit_copy.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_copy.Parent = pnl_edit_copy;
            btn_edit_copy.Location = new Point(0, 0);
            btn_edit_copy.Text = "Copy";
            btn_edit_copy.Dock = DockStyle.Fill;

            pnl_edit_paste.Location = new Point(0, 288);
            pnl_edit_paste.Size = new Size(255, 67);
            pnl_edit_paste.Parent = pnl_editMenu;
            pnl_edit_paste.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_paste.Parent = pnl_edit_paste;
            btn_edit_paste.Location = new Point(0, 0);
            btn_edit_paste.Text = "Paste";
            btn_edit_paste.Dock = DockStyle.Fill;

            pnl_edit_cut.Location = new Point(0, 355);
            pnl_edit_cut.Size = new Size(255, 67);
            pnl_edit_cut.Parent = pnl_editMenu;
            pnl_edit_cut.BackColor = Color.FromArgb(40, 40, 40);
            btn_edit_cut.Parent = pnl_edit_cut;
            btn_edit_cut.Location = new Point(0, 0);
            btn_edit_cut.Text = "Cut";
            btn_edit_cut.Dock = DockStyle.Fill;
            #endregion
        }

        private void tbx_codeTextChanged(object sender, FastColoredTextBoxNS.TextChangedEventArgs e) {
            Range range = e.ChangedRange;

            if (syntaxHighlighterJson != null) {
                range.ClearFoldingMarkers();

                for (int x = 0; x < syntaxHighlighterJson.FoldingMarkers.Count; x++) {
                    range.SetFoldingMarkers((string)syntaxHighlighterJson.FoldingMarkers[x].regexOpen, (string)syntaxHighlighterJson.FoldingMarkers[x].regexClose);
                }
                for (int x = 0; x < styles.Count; x++) {
                    range.ClearStyle(styles[x]);
                    range.SetStyle(styles[x], (string)syntaxHighlighterJson.Styles[x].regex, RegexOptions.Multiline);
                }
            }
        }
        private void Tbx_code_Load(object sender, EventArgs e) {
            loadNewSyntax("mcfunction");
        }

        private void loadNewSyntax(string name) {
            DirectoryInfo dir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\.gmasterhd\mceditor\syntaxHighlighter\");
            string fileName = "";

            foreach(FileInfo file in dir.GetFiles("*.json")) {
                dynamic json = JsonConvert.DeserializeObject(System.IO.File.ReadAllText(file.FullName));
                
                foreach(string end in json.FileEndings) {
                    if(end == name) {
                        fileName = file.FullName;
                    }
                }
            }
            
            if(fileName == "") {
                fileName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\.gmasterhd\mceditor\syntaxHighlighter\default.json";
            }
            styles = null;
            styles = new List<Style>();
            
            string jsonSRC = System.IO.File.ReadAllText(fileName);
            syntaxHighlighterJson = JsonConvert.DeserializeObject(jsonSRC);

            for (int x = 0; x < syntaxHighlighterJson.Styles.Count; x++) {
                Brush brush = new System.Drawing.SolidBrush(Color.FromArgb((int)syntaxHighlighterJson.Styles[x].Color[0], (int)syntaxHighlighterJson.Styles[x].Color[1], (int)syntaxHighlighterJson.Styles[x].Color[2]));
                Style style = new TextStyle(brush, null, FontStyle.Regular);

                styles.Add(style);
            }
        }

        private void Btn_file_newFile(object sender, EventArgs e) {
            new MCEditor.file.NewFile().Visible = true;
        }

        private void Tv_explorer_AfterSelect(object sender, TreeViewEventArgs e) {
        }

        private void BuildTree(DirectoryInfo directoryInfo, TreeNodeCollection addInMe) {
            TreeNode curNode = addInMe.Add(directoryInfo.Name);

            foreach (DirectoryInfo subdir in directoryInfo.GetDirectories()) {
                BuildTree(subdir, curNode.Nodes);
            }
            foreach (FileInfo file in directoryInfo.GetFiles()) {
                curNode.Nodes.Add(file.FullName, file.Name);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) {
        }

        #region File
        private void Btn_file_newWorkspace(object sender, EventArgs e) {
            fbd.ShowDialog();

            string filePath = fbd.SelectedPath;

            MCEditor.jsons.Workspace work = new MCEditor.jsons.Workspace();
            work.path = filePath;

            filePath = fbd.SelectedPath + "\\workspace.mcworkspace";

            string json = JsonConvert.SerializeObject(work);
            System.IO.File.WriteAllText(filePath, json);

            System.IO.Directory.CreateDirectory(fbd.SelectedPath + "\\Resourcepack\\Namespaces");
            System.IO.Directory.CreateDirectory(fbd.SelectedPath + "\\Datapack\\Namespaces");

            initExplorer(fbd.SelectedPath);
        }
        private void Btn_file_openWorkspace(object sender, EventArgs e) {
            ofd.Filter = "MCEditor Workspace | *.mcworkspace";
            ofd.FileName = "";
            ofd.ShowDialog();

            dynamic json = JsonConvert.DeserializeObject(System.IO.File.ReadAllText(ofd.FileName));
            initExplorer((string)json.path);
        }
        #endregion

        private void Tsm_open_Click(object sender, EventArgs e) {
            string filePath = explorerRoot.Substring(0, explorerRoot.LastIndexOf(@"\")) + @"\" + tv_explorer.SelectedNode.FullPath;

            Console.WriteLine(filePath.Substring(filePath.LastIndexOf(".") + 1));
            loadNewSyntax(filePath.Substring(filePath.LastIndexOf(".") + 1));

            tbx_code.Text = System.IO.File.ReadAllText(filePath);
        }

        public void open(string filePath) {
            open(filePath, filePath.Substring(0, filePath.LastIndexOf(".")));
        }
        public void open(string filePath, string ending) {
            instance.loadNewSyntax(ending);
            instance.tbx_code.Text = System.IO.File.ReadAllText(filePath);

            initExplorer(explorerRoot);
        }

        private void initExplorer(string path) {
            tv_explorer.Nodes.Clear();
            explorerRoot = path;
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            if (directoryInfo.Exists) {
                tv_explorer.AfterSelect += treeView1_AfterSelect;
                BuildTree(directoryInfo, tv_explorer.Nodes);
            }
        }

        private void Tv_explorer_MouseDoubleClick(object sender, MouseEventArgs e) {
            Console.WriteLine(explorerRoot.Substring(0, explorerRoot.LastIndexOf("\\") - 1) + tv_explorer.SelectedNode.FullPath);

            if(System.IO.File.Exists(explorerRoot.Substring(0, explorerRoot.LastIndexOf("\\")) + "\\" + tv_explorer.SelectedNode.FullPath)) {
                tbx_code.Parent.Text = tv_explorer.SelectedNode.Text;
                tbx_code.Text = System.IO.File.ReadAllText(explorerRoot.Substring(0, explorerRoot.LastIndexOf("\\")) + "\\" + tv_explorer.SelectedNode.FullPath);
            }
        }
    }
}
