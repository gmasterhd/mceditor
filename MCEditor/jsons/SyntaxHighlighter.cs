﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCEditor.jsons {
    public class SyntaxHighlighter {
        public string name;

        public Style[] Styles;
        public FoldingMarker[] FoldingMarkers;
        public string[] FileEndings;
    }
}
